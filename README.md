<h1>Static Blog System</h1>

This is the "static" blog system invented by Peter Nerlich and Arpit Kharbanda.

See it in use at peter.nerlich4u.de/blog

Do not use if you have no understanding of HTML, PHP and CSS. This is not aimed at people afraid of code. 

Disclaimer: Without warranty. Usage at own risk.